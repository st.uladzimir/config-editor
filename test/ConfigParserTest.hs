{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}

module ConfigParserTest (configParserTest) where

import Data.Proxy
import Test.Tasty

import Config.Types
import Utility

configParserTest :: TestTree
configParserTest =
    testGroup "Config Parsing"
        [
            readConf (Proxy :: Proxy SimpleConf) "simple.conf"
                `simpleTest`
            return [("bar","1"), ("baz","2"), ("foo","3"), ("quiz","4")]
          , readConf (Proxy :: Proxy SectionedConf) "sectioned.conf"
                `sectionTest`
            return
                [
                    ("sect1", [("foo","3"), ("bar","2")])
                  , ("sect2", [("baz","1")])
                  , ("sect3", [])
                ]
          , readConf (Proxy :: Proxy YamlConf) "yaml.conf"
                `yamlTest`
            return
                [
                    ("foo", yamlLeaf $ String "value 1")
                  , ("bar", yamlLeaf $ Number 1)
                  , ("baz", Subtrees [("quiz", yamlLeaf $ Bool True)])
                ]
        ]
            where
                yamlLeaf    = Leaf . Yaml
                readConf p  = fmap getParams . readConfig p