{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}

module Config.Edit
    (
        Config
      , clearStruct
      , delete
      , get
      , insert
      , parseConfig
      , readConfigFile
      , readSchemaFile
      , saveConfigToFile
      , serializeConfig
      , update
      , validate
    ) where


import Control.Applicative ((<|>))
import Control.Monad.Catch
import Control.Monad.Trans
import Data.List (partition)
import Data.Maybe (isJust, isNothing)
import Data.Proxy
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Yaml as Y
import Text.Megaparsec (eof)

import Config.Parser.Prim (runP)
import Config.Types



clearStruct :: (Monoid (FileStruct t k)) => Config t k v -> Config t k v
clearStruct cfg =
    cfg{getStruct = mempty}

insert :: (ParamTable t k) => k -> v -> t v -> t v
insert k v
    = alter (const $ Just v) k

delete :: (ParamTable t k) => k -> t v -> t v
delete
    = alter (const Nothing)

update :: (ParamTable t k) => (v -> v) -> k -> t v -> t v
update f
    = alter (fmap f)

validate :: (MonadThrow m, ParamTable t k, Show k) => ConfSchema t v -> Config t k v -> m (Config t k v)
validate schema cfg
    = if    null unset && null unknown
      then
            return cfg{getParams = table, getSchema = Just defaults}
      else
            throwM $ ValidationException (show <$> unset) (show <$> unknown)
        where
            defaults
                = paramsDescr schema
            defLst
                = fmap defaultValue <$> list defaults
            -- 'partition' parameters in schema on those
            -- that have default value and those that have none
            (yesDef,noDef)
                = partition (isJust . snd) defLst
            -- set all unset parameters to their default values
            table
                = foldl (\acc (par,mb_val) -> alter (<|> mb_val) par acc) (getParams cfg) yesDef
            -- 'filter' all parameters without default value that are absent in table
            unset
                = filter notInTable $ map fst noDef
            -- 'filter' all parameters in table that are absent in schema
            unknown
                = filter notInTable $ map fst defLst
            notInTable x
                = isNothing $ get x table

readSchemaFile :: (IsConfig t k v, MonadThrow m, MonadIO m) => Proxy (Config t k v) -> FilePath -> m (ConfSchema t v)
readSchemaFile _
    = Y.decodeFileThrow

parseConfig :: (IsConfig t k v, MonadThrow m) => String -> T.Text -> m (Config t k v)
parseConfig src
    = fmap (uncurry $ Config Nothing) . runP (parseTable <* eof) src

readConfigFile  :: (IsConfig t k v, MonadThrow m, MonadIO m)
                => FilePath
                -> m (Config t k v)
readConfigFile file
    = liftIO (T.readFile file)
        >>= parseConfig file

serializeConfig :: (IsConfig t k v) => Config t k v -> T.Text
serializeConfig (Config _ struct table)
    = serializeTable struct table

saveConfigToFile :: (IsConfig t k v, MonadIO m) => FilePath -> Config t k v -> m ()
saveConfigToFile file
    = liftIO . T.writeFile file . serializeConfig
