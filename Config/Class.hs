{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Config.Class
    (
        ConfSchema (..)
      , Empty (..)
      , JText (..)
      , ParamDescr (..)
      , ParamTable (..)
      , IsConfig (..)
      , TextKey (..)
      , TextValue (..)
      , Version (..)
      , (?!)
      , findDuplicates
      , popElem
      , withObjectOrNull
    ) where

import Control.Monad
import Data.Aeson
import Data.Aeson.Types (Parser, typeMismatch)
import Data.Bifunctor (first)
import Data.Char (isSpace)
import Data.Default (Default (def))
import Data.Either (rights)
import Data.Functor.Identity (Identity)
import Data.List (intercalate, partition)
import Data.Map (Map)
import qualified Data.Map as M
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified Data.Text.Lazy as LT
import qualified Data.Text.Lazy.Encoding as LT
import Data.Word (Word64)
import GHC.Exts
import Text.Megaparsec
import Text.Megaparsec.Char (char)

import qualified Config.Parser.Prim as P
import Data.NEList


class (Monoid (FileStruct t k), ParamTable t k, TextKey k, TextValue v) => IsConfig (t :: * -> *) k v | t -> k where
    type FileStruct t k :: *
    parseTable          :: P.Parser m (FileStruct t k, t v)
    serializeTable      :: FileStruct t k -> t v -> Text
    _schemaParser       :: Maybe Version -> Value -> Parser (t (ParamDescr v))



{- | simple config:
        param1=value1
        param2=value2
        ...
-}
instance IsConfig (Map Text) Text JText where
    -- |
    --  'Left' is for comments and empty lines, and
    --  'Right' - for parameter name
    type FileStruct (Map Text) Text
        = [Either Text Text]

    parseTable
        = do
            content <- iniParser
            let add el (struct,tbl)
                    = case el of
                        Left cmt    -> (Left cmt:struct,tbl)
                        Right (p,v) -> (Right p:struct,M.insert p (JText v) tbl)
            case findDuplicates (fst <$> rights content) of
                []  -> return $ foldr add ([],mempty) content
                xs  -> fail $ "Duplicated parameters: " <> intercalate ", " (map show xs)

    serializeTable struct table
        = T.unlines $ addRest $ foldr addElem ([],table) struct
            where
                addElem (Left x) acc
                    = first (x:) acc
                addElem (Right par) (xs,tbl)
                    = let (mb_val,tbl') = popElem par tbl
                      in
                        case mb_val of
                            Just v  -> (mkParLn par v:xs,tbl')
                            Nothing -> (xs,tbl')
                -- add parameters that are absent in structure,
                -- this may happen when you add new parameters
                addRest (xs,tbl_rest)
                    = xs <> M.foldrWithKey (\k v acc -> mkParLn k v : acc) [] tbl_rest
                mkParLn par val
                    = par <> "=" <> parValToTxt (getText val)
    _schemaParser v
        | Nothing <- v
            = _schemaParser $ Just curr_version
        | x == 0 && y == 1
            = withObject "simple schema" $ parseJSON . Object
        | otherwise
            = const $ fail $ "Unknown version: " <> show x <> "." <> show y
                where
                    ~(Just (Version x y))
                        = v
                    curr_version
                        = Version 0 1

iniParser :: P.Parser m [Either Text (Text,Text)]
iniParser
    = many $ eitherP P.nonParamLine P.paramLine

findDuplicates :: (Eq a) => [a] -> [a]
findDuplicates (x:xs)
    = let (xs',ys) = partition (/= x) xs
      in
        if  null ys
        then
            findDuplicates xs'
        else
            x:findDuplicates xs'
findDuplicates [] = []

popElem :: (Ord k) => k -> Map k v -> (Maybe v, Map k v)
popElem
    = M.updateLookupWithKey $ \_ _ -> Nothing

parValToTxt :: Text -> Text
parValToTxt txt
    -- when string contains double quote escape them with '\'
    -- and enclose string into double quotes
    | xs@(_:_:_) <- T.split (== '"') txt
        = enquote $ T.intercalate "\\\"" xs
    -- when string has space at the end or any Unicode newline character, enclose it in quotes
    | not . T.null P.&&& (isSpace . T.last P.||| T.any P.isLineEnd) $ txt
        = enquote txt
    | otherwise
        = txt
        where enquote = T.cons '"' . (`T.snoc` '"')



class (Empty t) => ParamTable (t :: * -> *) k | t -> k where
    alter   :: (Maybe v -> Maybe v) -> k -> t v -> t v
    get     :: k -> t v -> Maybe v
    list    :: t v -> [(k,v)]

instance (Ord k) => ParamTable (Map k) k where
    alter   = M.alter
    get     = M.lookup
    list    = M.toList



class Empty (t :: * -> *) where
    emptyT  :: t v
    default emptyT :: (Monoid (t v)) => t v
    emptyT  = mempty
instance (Ord k) => Empty (Map k)
instance Empty []


class (Show k) => TextKey k where
    readKey     :: String -> Either String k
    default readKey :: (FromJSON k) => String -> Either String k
    readKey     = eitherDecodeStrict . fromString

    keyToString :: k -> String
    default keyToString :: (Show k) => k -> String
    keyToString = show

instance TextKey Text where
    readKey
        = doParse P.identifier
    keyToString
        = T.unpack
instance TextKey (Text,Text) where
    readKey
        = doParse
            $ (,) <$> P.identifier <* char '/' <*> P.identifier
    keyToString (x,y)
        = T.unpack x <> "/" <> T.unpack y
instance TextKey (NEList Text) where
    readKey
        = doParse
            $ fromList <$> sepBy1 P.identifier (char '/')
    keyToString
        = intercalate "/" . map T.unpack . toList

doParse :: P.Parser Identity a -> String -> Either String a
doParse p txt
    = P.runPI (p <* eof) (shorten 20 txt) (T.pack txt)
        where
            shorten n s
                | [] <- s
                    = ""
                | n == 0
                    = "..."
                | (x:xs) <- s
                    = x:shorten (n - 1) xs



class TextValue v where
    readValue   :: String -> Either String v
    default readValue :: (FromJSON v) => String -> Either String v
    readValue   = eitherDecodeStrict . fromString

    valueToString   :: v -> String
    default valueToString :: (Show v) => v -> String
    valueToString   = show

instance TextValue JText where
    readValue
        = Right . JText . T.pack
    valueToString
        = T.unpack . getText

instance TextValue Value where
    readValue 
        = eitherDecodeStrict . T.encodeUtf8 . T.pack
    valueToString
        = LT.unpack . LT.decodeUtf8 . encode



newtype ConfSchema t v =
    ConfSchema {
        paramsDescr :: t (ParamDescr v)
    }
instance (Empty t) => Empty (ConfSchema t) where
    emptyT = ConfSchema emptyT
instance (IsConfig t k v) => FromJSON (ConfSchema t v) where
    parseJSON
        = withObject "schema"
            $ \o -> do
                        vsn <- o .:! "version"
                        res <- o .:! "parameters"
                        case res of
                            Nothing -> pure emptyT
                            Just v  -> ConfSchema <$> _schemaParser vsn v



data Version
    = Version {
            major   :: Word64
          , minor   :: Word64
        } deriving (Show)

instance FromJSON Version where
    parseJSON
        = withScientific "x.x"
            $ \n -> let (x,y) = (== '.') `break` show n
                    in  return $ Version (read x) (read $ tail y)

data ParamDescr v
    = ParamDescr {
            defaultValue :: Maybe v
          , description  :: Text
        } deriving (Show)

instance Empty ParamDescr where
    emptyT = ParamDescr Nothing ""

instance (FromJSON v) => FromJSON (ParamDescr v) where
    parseJSON
        = withObjectOrNull
            $ \o -> liftM2 ParamDescr
                        (o .:? "default-value")
                        (o ?! "description")



newtype JText
    = JText {
            getText :: Text
        } deriving (Eq, Ord)

instance Show JText where
    show = show . getText

instance IsString JText where
    fromString = JText . fromString

instance FromJSON JText where
    parseJSON (String txt)
        = return $ JText txt
    parseJSON v@(Object _)
        = typeMismatch "not an object" v
    parseJSON v
        = return . JText . LT.toStrict . LT.decodeUtf8 . encode $ v

withObjectOrNull :: (Empty t) => (Object -> Parser (t a)) -> Value -> Parser (t a)
withObjectOrNull p v
    = case v of
        Null    -> pure emptyT
        _       -> withObject "Object or Null" p v

-- |
-- @
--  o ?! s == o '.:!' s '.!=' 'def'@
-- @
(?!) :: (Default v, FromJSON v) => Object -> Text -> Parser v
o ?! s
    = o .:! s .!= def

instance Default Text where
    def = mempty

instance Default Value where
    def = Null
