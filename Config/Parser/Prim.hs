{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}

module Config.Parser.Prim where

import Control.Monad.Catch hiding (try)
import Control.Monad.Identity
import Data.Bifunctor (first)
import Data.Char
import Data.Text (Text)
import qualified Data.Text as T
import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char

type Parser = ParsecT Void Text

data ParsingError
    = ParsingError String
        deriving (Show)
instance Exception ParsingError where
    displayException (ParsingError s) = s

paramLine :: Parser m (Text,Text)
paramLine =
    try $ spaceH >> liftM2 (,) identifier (spaceH >> char '=' >> paramValue)

identifier :: (Stream s, Token s ~ Char) => ParsecT Void s m (Tokens s)
identifier =
    label "identifier" $ do
        lookAhead $ satisfy (isAsciiLetter ||| (== '_'))
        takeWhileP Nothing $ isAlNumAscii ||| (== '_')

paramValue :: Parser m Text
paramValue =
    spaceH
        *> (quotedString <|> textLine)
        <* (lineEnd <|> eof)

nonParamLine :: Parser m Text
nonParamLine = try $ do
    spaceH
    commentLine <|> (lineEnd >> return "")

commentLine :: Parser m Text
commentLine =
    spaceH
        *> lookAhead (char '#')
        *> textLine
        <* (lineEnd <|> eof)

quotedString :: Parser m Text
quotedString =
    let getText = do
            -- take text until the closing quote or backslash
            chk1 <- takeWhileP Nothing $ (/= '"') &&& (/= '\\')
            -- in case of backslash followed by double quote treat this as double quote,
            -- otherwise process backslash as normal char,
            -- and continue
            (do
                char '\\'
                nextChar <- char '"' <|> return '\\'
                chk2 <- getText
                return $ T.snoc chk1 nextChar <> chk2)
            -- when stopped on double quote finish parsing
                    <|> return chk1
    in
        between (char '"') (char '"') getText

-- | parses text up to newline, skipping trailing space
textLine :: Parser m Text
textLine =
    fmap (T.dropWhileEnd isSpace) (takeWhileP Nothing (/= '\n'))

-- | any space, but not newline
spaceH :: Parser m Text
spaceH =
    takeWhileP Nothing $ isSpace &&& (/= '\n')

lineEnd :: Parser m ()
lineEnd = spaceH >> void newline

runP :: (MonadThrow m) => Parser m a -> String -> Text -> m a
runP p src =
    either (throwM . ParsingError . parseErrorPretty) return
        <=< runParserT p src

runPI :: Parser Identity a -> String -> Text -> Either String a
runPI p src =
    first parseErrorPretty . runParser p src

isAsciiLetter, isAlNumAscii, isLineEnd :: Char -> Bool
isAsciiLetter   = isAsciiLower ||| isAsciiUpper
isAlNumAscii    = isAsciiLetter ||| isDigit
isLineEnd       = flip elem ("\n\v\f\r\x85\x2028\x2029" :: String)

(|||),(&&&) :: (a -> Bool) -> (a -> Bool) -> (a -> Bool)
f ||| g = \x -> f x || g x
f &&& g = \x -> f x && g x
infixr 1 |||, &&&
