{- |
    sectioned config
        [section1]
        param1=value1
        param2=value2
        [section2]
        ...
-}

{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}

module Config.SectionTable where

import Control.Monad
import Data.Aeson
import Data.Bifunctor (bimap, first)
import Data.List (intercalate)
import Data.Map (Map)
import qualified Data.Map as M
import Data.Text (Text)
import qualified Data.Text as T
import GHC.Exts (IsList (..))
import Text.Megaparsec
import Text.Megaparsec.Char

import Config.Class
import qualified Config.Parser.Prim as P



newtype SectionTable k v
    = SectionTable {
            getSections :: Map k (Map k v)
        } deriving (Eq, FromJSON, Show)

instance Functor (SectionTable k) where
    fmap f
        = SectionTable . fmap (fmap f) . getSections

instance (Ord k) => IsList (SectionTable k v) where
    type Item (SectionTable k v)
        = (k, Map k v)
    fromList
        = SectionTable . fromList
    toList
        = toList . getSections

instance (Ord k) => Empty (SectionTable k) where
    emptyT = SectionTable mempty

instance (Ord k) => ParamTable (SectionTable k) (k,k) where
    alter f (sect,par)
        = SectionTable . M.alter updSect sect . getSections
            where
                updSect s
                    | Just x <- s
                        = Just $ M.alter f par x
                    | Just v <- f Nothing
                        = Just $ M.singleton par v
                    | otherwise
                        = Nothing
    get (sect,par)
        = M.lookup sect . getSections >=> M.lookup par
    list
        = M.toList . getSections
            >=> \(sect,tbl) -> first ((,) sect) <$> M.toList tbl

instance IsConfig (SectionTable Text) (Text,Text) JText where
    type FileStruct (SectionTable Text) (Text,Text)
        = SectionedStruct
    parseTable
        = do
            header  <- many P.nonParamLine
            sects   <- many $ do
                                name <- sectionHeaderP
                                (struct,tbl) <- parseTable
                                return (name,struct,tbl)
            let process (sec,sec_struct,sec_tbl) (xs,tbl)
                    = ((sec,sec_struct):xs, M.insert sec sec_tbl tbl)
            case findDuplicates $ map (\(sect,_,_) -> sect) sects of
                []  -> return
                            $ bimap (SectionedStruct header) SectionTable
                            $ foldr process (mempty,mempty) sects
                xs  -> fail $ "Duplicated sections: " <> intercalate ", " (map show xs)

    serializeTable (SectionedStruct hdr sects_struct) (SectionTable table)
        = T.unlines hdr <> (addRest $ foldr addSect (mempty,table) sects_struct)
            where
                addSect (name,sub_struct) (content,tbl)
                    = let (mb_tbl,tbl') = popElem name tbl
                      in
                        case mb_tbl of
                            Just sub_tbl    -> (serializeSection name sub_struct sub_tbl <> content,tbl')
                            Nothing         -> (content,tbl')
                -- similar to '(Map Text) Text Text' instance in 'Config.Internal.Class'
                addRest (content,tbl_rest)
                    = content <> M.foldrWithKey (\k v acc -> serializeSection k [] v <> acc) mempty tbl_rest
    _schemaParser v
        | Nothing <- v
            = _schemaParser $ Just curr_version
        | x == 0 && y == 1
            = withObject "section schema" $ parseJSON . Object
        | otherwise
            = const $ fail $ "Unknown version: " <> show x <> "." <> show y
                where
                    ~(Just (Version x y))
                        = v
                    curr_version
                        = Version 0 1



data SectionedStruct
    = SectionedStruct {
            _header :: [Text]
          , _sects  :: [(Text,[Either Text Text])] 
        } deriving (Show)
instance Semigroup SectionedStruct where
    (SectionedStruct h1 t1) <> (SectionedStruct h2 t2)
            = SectionedStruct (h1 <> h2) (t1 <> t2)
instance Monoid SectionedStruct where
    mempty
        = SectionedStruct mempty mempty

serializeSection :: Text -> FileStruct (Map Text) Text -> Map Text JText -> Text
serializeSection sect struct tbl
    = "[" <> sect <> "]\n" <> serializeTable struct tbl

sectionHeaderP :: P.Parser m Text
sectionHeaderP
    = P.spaceH
        *> char '['
        *> P.spaceH
        *> P.identifier
        <* P.spaceH
        <* char ']'
        <* (P.lineEnd <|> eof)
